FROM node:18.15 as test

WORKDIR /app

# Copier le fichier package.json et le fichier package-lock.json dans le conteneur
COPY package*.json ./

# Installer les dépendances uniquement si le package.json a changé
RUN npm install

COPY . .

# Lancer les tests
RUN npm test

FROM node:18.15 as lint

WORKDIR /app

# Copier le fichier package.json et le fichier package-lock.json dans le conteneur
COPY package*.json ./

# Installer les dépendances uniquement si le package.json a changé
RUN npm install

COPY . .

# Lancer les tests
RUN npm run lint

# Utilisez une image de node.js comme base
FROM node:18.15 as run

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le fichier package.json et le fichier package-lock.json dans le conteneur
COPY package*.json ./

# Installer les dépendances uniquement si le package.json a changé
RUN npm install

# Copier les fichiers source de l'application dans le conteneur
COPY . .

# Exposer le port 4200
EXPOSE 4200

# Démarrer l'application
CMD ["npm", "run", "start"]
