import {Component} from '@angular/core';
import { Location } from '@angular/common';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  constructor(
    private readonly location: Location,
    private readonly router: Router,
  ) {
    location.onUrlChange((url) => {
      this.url = url
    })

  }
  title = 'characters-builder';
  url = '';

  onClick() {
    this.router.navigate(["history"])
  }

  signUp() {
    this.router.navigate(["sign-up"])
  }
}
