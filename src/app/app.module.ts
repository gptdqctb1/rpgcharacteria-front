import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import { LoginComponent } from './login/login.component';
import { BuilderCharacterComponent } from './builder-character/builder-character.component';
import {RouterModule} from "@angular/router";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {AngularFireAuthModule} from "@angular/fire/compat/auth";
import {AngularFireModule} from "@angular/fire/compat";
import {MatRadioModule} from "@angular/material/radio";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import { HttpClientModule } from '@angular/common/http';
import {HistoryComponent} from "./history/history.component";
import { CardCharacterComponent } from './components/card-character/card-character.component';
import {environment} from "../environments/environment.character-builder";
import { SignUpComponent } from './sign-up/sign-up.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatSnackBarModule} from "@angular/material/snack-bar";

const firebaseConfig = {
  apiKey: environment.envVar.API_KEY_FIREBASE,
  authDomain: environment.envVar.AUTH_DOMAIN_FIREBASE,
  projectId: environment.envVar.PROJECT_ID,
  storageBucket: environment.envVar.STORAGE_BUCKET_FIREBASE,
  messagingSenderId: environment.envVar.MESSAGING_SENDER_FIREBASE,
  appId: environment.envVar.APP_ID_FIREBASE
};

console.log(firebaseConfig)
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BuilderCharacterComponent,
    HistoryComponent,
    CardCharacterComponent,
    SignUpComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSnackBarModule,
        RouterModule.forRoot([
            {path: "", component: LoginComponent},
            {path: "builder", component: BuilderCharacterComponent},
            {path: "history", component: HistoryComponent},
            {path: "sign-up", component: SignUpComponent},
            {path: "**", component: LoginComponent},
        ]),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireAuthModule,
        MatFormFieldModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
        MatRadioModule,
        MatCheckboxModule,
        MatDividerModule,
        HttpClientModule,
        MatProgressSpinnerModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
