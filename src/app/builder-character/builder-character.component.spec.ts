import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderCharacterComponent } from './builder-character.component';
import {HttpClientModule} from "@angular/common/http";
import {MatSnackBarModule} from "@angular/material/snack-bar";

describe('BuilderCharacterComponent', () => {
  let component: BuilderCharacterComponent;
  let fixture: ComponentFixture<BuilderCharacterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuilderCharacterComponent ],
      imports: [HttpClientModule, MatSnackBarModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderCharacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
