import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { FetchApiService } from "../services/fetch-api.service";
import { Character, JeSaisPas } from "../types/character";
import { BehaviorSubject, forkJoin, Observable, Subject } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-builder-character',
  templateUrl: './builder-character.component.html',
  styleUrls: ['./builder-character.component.scss']
})
export class BuilderCharacterComponent implements OnInit {

  constructor(
    private readonly fetchApiService: FetchApiService,
    private _snackBar: MatSnackBar
  ) {
  }


  subjectCharacter: Subject<{ name: string, description: string, image: string }> =
    new BehaviorSubject<{ name: string, description: string, image: string }>({ name: '', image: '', description: '' });
  observableCharacter: Observable<{ name: string, description: string, image: string }> = this.subjectCharacter as Observable<{ name: string, description: string, image: string }>;

  name: string = "";
  description: string = "";
  image: string = "";
  isLoading = false;

  builderForm = new FormGroup({
    genre: new FormControl("male", Validators.required),
    caracteristic: new FormGroup({
      borgne: new FormControl(false),
      estropier: new FormControl(false),
      handicaper: new FormControl(false),
      muscler: new FormControl(false),
      sourd: new FormControl(false)
    }),
    race: new FormGroup({
      humain: new FormControl(true),
      nain: new FormControl(false),
      dragon: new FormControl(false),
      elfe: new FormControl(false),
      orc: new FormControl(false),
    }, Validators.required),
    metier: new FormGroup({
      forgeron: new FormControl(true),
      tanneur: new FormControl(false),
      cuisinier: new FormControl(false),
      alchimiste: new FormControl(false),
      architecte: new FormControl(false),
    }, Validators.required),
    classe: new FormGroup({
      mage: new FormControl(true),
      paladin: new FormControl(false),
      voleur: new FormControl(false),
      guerrier: new FormControl(false),
      archer: new FormControl(false),
    }, Validators.required),
  }
  )


  ngOnInit(): void {
    this.observableCharacter.subscribe({
      next: (result) => {
        this.isLoading = false
        this.name = result.name
        this.description = result.description
        this.image = result.image
      },
      error: (e) => {
        console.log(e)
        this.isLoading = false
      }
    })
  }

  onSubmit() {
    this.name = ""
    this.description = ""
    this.image = ""
    this.isLoading = true

    const jeSaispas: JeSaisPas = {
      // @ts-ignore
      metier: this.getElement(this.builderForm.get("metier").value),
      // @ts-ignore
      classe: this.getElement(this.builderForm.get("classe").value),
      // @ts-ignore
      race: this.getElement(this.builderForm.get("race").value),
      // @ts-ignore
      caracteristic: this.getElement(this.builderForm.get("caracteristic").value),

      // @ts-ignore
      genre: this.builderForm.get("genre").value,
    }

    forkJoin([this.fetchApiService.getImage(jeSaispas), this.fetchApiService.getDescription(jeSaispas)])
      .subscribe({
        next: ([dataImage, dataStory]) => {
          this.subjectCharacter.next({
            name: dataStory.name,
            description: dataStory.description,
            image: dataImage.image
          })
        },
        error: (e) => {
          console.log(e)
          this.isLoading = false
        }
      })
  }
  getElement(elements: { [key: string]: boolean }): string[] {
    const list: string[] = []
    Object.entries(elements).forEach((element) => {
      if (element[1]) {
        list.push(element[0])
      }
    })
    return list
  }

  onClick() {
    const character: Character = {
      description: this.description,
      name: this.name,

      image: this.image
    }
    this.fetchApiService.addCharacter(character).subscribe({
      next: (result) => {
        this._snackBar.open('Personnage sauvegardé', null, { duration: 5000 })
      },
      error: (err) => {
        this._snackBar.open('Erreur sauvegarde')
      }
    })

  }
}
