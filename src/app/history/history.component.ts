import { Component, OnInit } from '@angular/core';
import {FetchApiService} from "../services/fetch-api.service";
import {Character} from "../types/character";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(
    private readonly fetchApiService: FetchApiService
  ) { }

  characters: Character[] = []
  ngOnInit(): void {
    this.fetchApiService.getHistory().subscribe((result: Character[]) => {
      this.characters = result.reverse()
    })
  }

}
