import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {Router} from "@angular/router";
import {StockTokenService} from "../services/stock-token.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private readonly afAuth: AngularFireAuth,
    private readonly router: Router,
    private readonly stockTokenSevice: StockTokenService
  ) {
  }

  hide = true;

  loginForm = new FormGroup({
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required),
  });

  get email() {
    return this.loginForm.get('email') as FormControl;
  }

  get password() {
    return this.loginForm.get('password') as FormControl;
  }

  ngOnInit(): void {

  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.afAuth.signInWithEmailAndPassword(this.email.value, this.password.value).then(() => {
          this.afAuth.idToken.subscribe(result => {
            if (result) {
              this.stockTokenSevice.setToken(result)
              this.router.navigate(['builder'])
            }
          })
        }
      )
    }
  }
}
