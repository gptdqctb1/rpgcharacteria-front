import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Character, Description, JeSaisPas } from "../types/character";
import { mock } from "../../assets/config";
import { StockTokenService } from "./stock-token.service";
import { ip } from "../const/config";

@Injectable({
  providedIn: 'root'
})
export class FetchApiService {

  constructor(
    private readonly http: HttpClient,
    private readonly token: StockTokenService
  ) {
  }

  getDescription(jeSaispas: JeSaisPas): Observable<Description> {
    const params: HttpParams = new HttpParams()
      .append("metier", jeSaispas.metier.toString())
      .append("classe", jeSaispas.classe.toString())
      .append("genre", jeSaispas.genre)
      .append("race", jeSaispas.race.toString())
      .append("caracteristic", jeSaispas.caracteristic.toString())
    const headers: HttpHeaders = new HttpHeaders().append(
      "Authorization", `Bearer ${this.token.getToken()}`
    ).set('Content-Type', 'application/json')
    if (mock) {
      return of({
        name: "toto",
        description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
      })
    }
    return this.http.get<Description>(`https://${ip}/story/`, { params, headers })
  }

  getImage(jeSaispas: JeSaisPas): Observable<{ image: string }> {
    const params: HttpParams = new HttpParams()
      .append("metier", jeSaispas.metier.toString())
      .append("classe", jeSaispas.classe.toString())
      .append("genre", jeSaispas.genre)
      .append("race", jeSaispas.race.toString())
      .append("caracteristic", jeSaispas.caracteristic.toString())

    const headers: HttpHeaders = new HttpHeaders().append(
      "Authorization", `Bearer ${this.token.getToken()}`
    ).set('Content-Type', 'application/json')
    if (mock) {
      return of({ image: "https://www.abondance.com/wp-content/uploads/2018/07/pirates-des-caraibes.jpg" })
    }
    return this.http.get<{ image: string }>(`https://${ip}/image/`, { params, headers })
  }

  getHistory(): Observable<Character[]> {
    const headers: HttpHeaders = new HttpHeaders().append(
      "Authorization", `Bearer ${this.token.getToken()}`
    ).set('Content-Type', 'application/json')
    if (mock) {
      return of([
        {
          name: "toto",
          description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.",
          image: "https://www.abondance.com/wp-content/uploads/2018/07/pirates-des-caraibes.jpg",
        },
        {
          name: "toto",
          description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.",
          image: "https://www.abondance.com/wp-content/uploads/2018/07/pirates-des-caraibes.jpg",
        },
        {
          name: "toto",
          description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.",
          image: "https://www.abondance.com/wp-content/uploads/2018/07/pirates-des-caraibes.jpg",
        }, {
          name: "toto",
          description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.",
          image: "https://www.abondance.com/wp-content/uploads/2018/07/pirates-des-caraibes.jpg",
        },

      ])
    }
    return this.http.get<Character[]>(`https://${ip}/book/`, { headers })
  }

  addCharacter(character: Character): Observable<Character> {
    const headers: HttpHeaders = new HttpHeaders().append(
      "Authorization", `Bearer ${this.token.getToken()}`
    ).set('Content-Type', 'application/json')
    return this.http.post<Character>(`https://${ip}/book/`, character, { headers })
  }


}
