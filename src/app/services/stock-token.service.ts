import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class StockTokenService {

  setToken(token: string) {
    localStorage.setItem('token', JSON.stringify(token))
  }

  getToken(): string {
    return JSON.parse(localStorage.getItem('token'))
  }
}
