import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {Router} from "@angular/router";
import {StockTokenService} from "../services/stock-token.service";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor(
    private readonly afAuth: AngularFireAuth,
    private readonly router: Router,
    private readonly stockTokenSevice: StockTokenService
  ) {
  }

  hide = true;

  signUpForm = new FormGroup({
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required),
  });

  get email() {
    return this.signUpForm.get('email') as FormControl;
  }

  get password() {
    return this.signUpForm.get('password') as FormControl;
  }

  onSubmit() {
    if (this.signUpForm.valid) {
      this.afAuth.createUserWithEmailAndPassword(this.email.value, this.password.value).then(() => {
          this.afAuth.idToken.subscribe(result => {
            if (result) {
              this.stockTokenSevice.setToken(result)
              this.router.navigate(['builder'])
            }
          })
        }
      )
    }
  }

  ngOnInit(): void {
  }

}
