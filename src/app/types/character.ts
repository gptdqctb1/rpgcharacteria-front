export interface Character {
  name: string;
  description: string;
  image: string;
}
export interface Description {
  name: string;
  description: string;
}

export interface JeSaisPas {
  metier: string[];
  classe: string[];
  genre: string;
  race: string[];
  caracteristic: string[];
}
