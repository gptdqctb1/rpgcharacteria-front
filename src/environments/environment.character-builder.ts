
// @ts-ignore
export const environment = {
  production: false,
  envVar: {
    // @ts-ignore

    API_KEY_FIREBASE: process.env.NG_APP_API_KEY_FIREBASE,
    // @ts-ignore

    AUTH_DOMAIN_FIREBASE: process.env.NG_APP_AUTH_DOMAIN_FIREBASE,
    // @ts-ignore

    PROJECT_ID: process.env.NG_APP_PROJECT_ID,
    // @ts-ignore

    STORAGE_BUCKET_FIREBASE: process.env.NG_APP_STORAGE_BUCKET_FIREBASE,
    // @ts-ignore

    MESSAGING_SENDER_FIREBASE: process.env.NG_APP_MESSAGING_SENDER_FIREBASE,
    // @ts-ignore

    APP_ID_FIREBASE: process.env.NG_APP_APP_ID_FIREBASE,

    // @ts-ignore
    IP: process.env.NG_APP_IP
  }
};
